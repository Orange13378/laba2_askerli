using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.IO;


namespace Laba2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public int list = 1;
        public bool outs = true;
        public bool shorts;
        public bool upd;

        string lab2 = @"\Laba2.xlsx";
        string lab1 = @"\Laba2_1.xlsx";
        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location); //Файл Excel сохраняется в Laba2\Laba2\bin\Debug папке проекта


        public MainWindow()
        {
            InitializeComponent();

            if (File.Exists(path + lab2))
            {
                Test[] test1 = EPPlus.ReadExcel(path + lab2);

                Test[] test2 = new Test[10];
                int k = 0;
                for (int i = (list - 1) * 10; i < 10 * list; i++)
                {
                    test2[k] = test1[i];
                    k++;
                }


                Exel_Grid.ItemsSource = test2;
                Exel_Grid.Items.Refresh();
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Файл не найден, скачать с интернета?", "Файл не найден", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    WebClient client = new WebClient();
                    client.DownloadFile("https://bdu.fstec.ru/files/documents/thrlist.xlsx", "Laba2.xlsx");
                    Test[] test1 = EPPlus.ReadExcel(path + lab2);

                    Test[] test2 = new Test[10];
                    int k = 0;
                    for (int i = (list - 1) * 10; i < 10 * list; i++)
                    {
                        test2[k] = test1[i];
                        k++;
                    }
                    Exel_Grid.ItemsSource = test2;
                    Exel_Grid.Items.Refresh();
                }
                else
                {
                    MessageBox.Show("Жаль, может потом(");
                    this.Close();
                }
            }
        }

        private void Next(object sender, RoutedEventArgs e)
        {
            upd = false;
            if (!shorts)
            {

                Test[] test1 = EPPlus.ReadExcel(path + lab2); ;
                if (list < test1.Length / 10)
                {
                    list++;
                    Test[] test2 = new Test[10];
                    int k = 0;
                    for (int i = (list - 1) * 10; i < 10 * list; i++)
                    {
                        test2[k] = test1[i];
                        k++;
                    }
                    Exel_Grid.ItemsSource = test2;
                    Exel_Grid.Items.Refresh();
                    outs = true;
                }
                else
                {
                    if ((test1.Length % 10 != 0) && (outs))
                    {
                        Test[] test2 = new Test[test1.Length % 10];
                        int k = 0;
                        for (int i = list * 10; i < 10 * list + test1.Length % 10; i++)
                        {
                            test2[k] = test1[i];
                            k++;
                        }
                        Exel_Grid.ItemsSource = test2;
                        Exel_Grid.Items.Refresh();
                        outs = false;
                    }
                    else
                    {
                        MessageBox.Show("Больше страниц нет");
                    }
                }
            }
            else
            {
                Test2[] test1 = EPPlus.ShortReadExcel(path + lab2); ;
                if (list < test1.Length / 10)
                {
                    list++;
                    Test2[] test2 = new Test2[10];
                    int k = 0;
                    for (int i = (list - 1) * 10; i < 10 * list; i++)
                    {
                        test2[k] = test1[i];
                        k++;
                    }
                    Exel_Grid.ItemsSource = test2;
                    Exel_Grid.Items.Refresh();
                    outs = true;
                }
                else
                {
                    if ((test1.Length % 10 != 0) && (outs))
                    {
                        Test2[] test2 = new Test2[test1.Length % 10];
                        int k = 0;
                        for (int i = list * 10; i < 10 * list + test1.Length % 10; i++)
                        {
                            test2[k] = test1[i];
                            k++;
                        }
                        Exel_Grid.ItemsSource = test2;
                        Exel_Grid.Items.Refresh();
                        outs = false;
                    }
                    else
                    {
                        MessageBox.Show("Больше страниц нет");
                    }
                }
            }
        }

        private void Last(object sender, RoutedEventArgs e)
        {
            upd = false;
            if (!shorts)
            {
                Test[] test1 = EPPlus.ReadExcel(path + lab2);

                if ((list > 1) && (list < test1.Length / 10))
                {
                    list--;
                    Test[] test2 = new Test[10];
                    int k = 0;
                    for (int i = (list - 1) * 10; i < 10 * list; i++)
                    {
                        test2[k] = test1[i];
                        k++;
                    }
                    Exel_Grid.ItemsSource = test2;
                    Exel_Grid.Items.Refresh();
                }
                else
                {
                    if (((list > 1)) && (list == test1.Length / 10))
                    {
                        list--;
                        Test[] test2 = new Test[10];
                        int k = 0;
                        for (int i = list * 10; i < 10 * (list + 1); i++)
                        {
                            test2[k] = test1[i];
                            k++;
                        }
                        Exel_Grid.ItemsSource = test2;
                        Exel_Grid.Items.Refresh();
                    }
                    else
                    {
                        MessageBox.Show("Меньше страниц нет");
                    }
                }
            }
            else
            {
                Test2[] test1 = EPPlus.ShortReadExcel(path + lab2);

                if ((list > 1) && (list < test1.Length / 10))
                {
                    list--;
                    Test2[] test2 = new Test2[10];
                    int k = 0;
                    for (int i = (list - 1) * 10; i < 10 * list; i++)
                    {
                        test2[k] = test1[i];
                        k++;
                    }
                    Exel_Grid.ItemsSource = test2;
                    Exel_Grid.Items.Refresh();
                }
                else
                {
                    if (list == test1.Length / 10)
                    {
                        list--;
                        Test2[] test2 = new Test2[10];
                        int k = 0;
                        for (int i = list * 10; i < 10 * (list + 1); i++)
                        {
                            test2[k] = test1[i];
                            k++;
                        }
                        Exel_Grid.ItemsSource = test2;
                        Exel_Grid.Items.Refresh();
                    }
                    else
                    {
                        MessageBox.Show("Меньше страниц нет");
                    }
                }
            }
        }

        private void Exel_Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (shorts && (upd == false))
                {
                    if (sender != null)
                    {
                        DataGrid grid = sender as DataGrid;
                        if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                        {
                            DataGridRow dgr = grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem) as DataGridRow;
                            Test2 dr = (Test2)dgr.Item;

                            string ProductId = dr.Id.ToString();
                            string ProductName = dr.Name.ToString();

                            MessageBox.Show("УБИ:" + ProductId + "\n\nИмя угрозы:\n\n" + ProductName, "Информация о строке");
                        }
                    }
                }
                else if (!shorts && (upd == false))
                {
                    if (sender != null)
                    {
                        DataGrid grid = sender as DataGrid;
                        if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                        {
                            DataGridRow dgr = grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem) as DataGridRow;
                            Test dr = (Test)dgr.Item;

                            string ProductId = dr.Id.ToString();
                            string ProductName = dr.Name.ToString();
                            string ProductDescription = dr.Discription.ToString();
                            string ProductSource = dr.Source.ToString();
                            string ProductObject = dr.Object.ToString();
                            string ProductPrivacyError = dr.PrivacyError.ToString();
                            string ProductCompleteError = dr.CompleteError.ToString();
                            string ProductAccessError = dr.AccessError.ToString();
                            MessageBox.Show("Id угрозы: " + ProductId + "\n\nИмя угрозы:\n" + ProductName + "\n\nОписание :\n" + ProductDescription + "\n\nИсточник угрозы :\n" + ProductSource + "\n\nОбъект воздействия угрозы :\n" + ProductObject + "\n\nНарушение конфиденциальности : " + ProductPrivacyError + "\n\nНарушение целостности : " + ProductCompleteError + "\n\nНарушение доступности : " + ProductAccessError, "Информация о строке");
                        }
                    }
                }
                else if (upd == true)
                {
                    if (sender != null)
                    {
                        DataGrid grid = sender as DataGrid;
                        if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                        {
                            DataGridRow dgr = grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem) as DataGridRow;
                            Update dr = (Update)dgr.Item;

                            string ProductId = dr.Ids.ToString();
                            string ProductLast = dr.Last.ToString();
                            string ProductNext = dr.Next.ToString();
                            MessageBox.Show("Строка: " + ProductId + "\n\nБыло:\n" + ProductLast + "\n\nСтало :\n" + ProductNext, "Информация о строке");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void Shorts_View(object sender, RoutedEventArgs e)
        {
            upd = false;
            if (File.Exists(path + lab2))
            {
                Test2[] test1 = EPPlus.ShortReadExcel(path + lab2);

                Test2[] test2 = new Test2[10];
                int k = 0;
                for (int i = (list - 1) * 10; i < 10 * list; i++)
                {
                    test2[k] = test1[i];
                    k++;
                }

                shorts = true;
                Exel_Grid.ItemsSource = test2;
                Exel_Grid.Items.Refresh();
            }
            else
            {
                MessageBox.Show("Файла нет!!!", "Ошибка");
            }
        }

        private void Longs_View(object sender, RoutedEventArgs e)
        {
            upd = false;
            if (File.Exists(path + lab2))
            {
                Test[] test1 = EPPlus.ReadExcel(path + lab2);

                Test[] test2 = new Test[10];
                int k = 0;
                for (int i = (list - 1) * 10; i < 10 * list; i++)
                {
                    test2[k] = test1[i];
                    k++;
                }

                shorts = false;
                Exel_Grid.ItemsSource = test2;
                Exel_Grid.Items.Refresh();
            }
            else
            {
                MessageBox.Show("Файла нет!!!", "Ошибка");
            }
        }

        private void Exel_Grid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (!shorts)
            {
                switch (e.PropertyName)
                {
                    case "Id":
                        e.Column.Header = "Идентификатор угрозы";
                        break;

                    case "Name":
                        e.Column.Header = "Наименование угрозы";
                        break;

                    case "Discription":
                        e.Column.Header = "Описание угрозы";
                        break;

                    case "Source":
                        e.Column.Header = "Источник угрозы";
                        break;

                    case "Object":
                        e.Column.Header = "Объект воздействия угрозы";
                        break;

                    case "PrivacyError":
                        e.Column.Header = "Нарушение конфиденциальности";
                        break;

                    case "CompleteError":
                        e.Column.Header = "Нарушение целостности";
                        break;

                    case "AccessError":
                        e.Column.Header = "Нарушение доступности";
                        break;

                    default:
                        break;
                }
            }
            else if (shorts)
            {
                switch (e.PropertyName)
                {
                    case "Id":
                        e.Column.Header = "УБИ";
                        break;

                    case "Name":
                        e.Column.Header = "Наименование угрозы";
                        break;

                    default:
                        break;
                }
            }
            
            if (upd == true)
            {
                switch (e.PropertyName)
                {
                    case "Ids":
                        e.Column.Header = "Строка";
                        break;

                    case "Next":
                        e.Column.Header = "Стало";
                        break;

                    case "Last":
                        e.Column.Header = "Было";
                        break;

                    default:
                        break;
                }
            }
        }

        private void Check_Update(object sender, RoutedEventArgs e)
        {
            try
            {
                upd = true;
                WebClient client = new WebClient(); 

                if (File.Exists(path + lab2))
                {
                    if (!File.Exists(path + lab1))
                    {
                        client.DownloadFile("https://bdu.fstec.ru/files/documents/thrlist.xlsx", "Laba2_1.xlsx");
                        Test[] test2 = EPPlus.ReadExcel(path + lab1); //на инета

                        Test[] test1 = EPPlus.ReadExcel(path + lab2); //с компа
                        List<Update> test5 = new List<Update>();
                        int up = 0;

                        for (int j = 0; j < test1.Length; j++)
                        {
                            if (test1[j].Id != test2[j].Id)
                            {
                                test5.Add(new Update() { Ids = (j + 1).ToString(), Last = test1[j].Id, Next = test2[j].Id });
                                up++;
                            }

                            if (test1[j].Name != test2[j].Name)
                            {
                                test5.Add(new Update() { Ids = (j + 1).ToString(), Last = test1[j].Name, Next = test2[j].Name });
                                up++;
                            }

                            if (test1[j].Discription != test2[j].Discription)
                            {
                                test5.Add(new Update() { Ids = (j + 1).ToString(), Last = test1[j].Discription, Next = test2[j].Discription });
                                up++;
                            }

                            if (test1[j].Source != test2[j].Source)
                            {
                                test5.Add(new Update() { Ids = (j + 1).ToString(), Last = test1[j].Source, Next = test2[j].Source });
                                up++;
                            }

                            if (test1[j].Object != test2[j].Object)
                            {
                                test5.Add(new Update() { Ids = (j + 1).ToString(), Last = test1[j].Object, Next = test2[j].Object });
                                up++;
                            }

                            if (test1[j].PrivacyError != test2[j].PrivacyError)
                            {
                                test5.Add(new Update() { Ids = (j + 1).ToString(), Last = test1[j].PrivacyError, Next = test2[j].PrivacyError });
                                up++;
                            }

                            if (test1[j].CompleteError != test2[j].CompleteError)
                            {
                                test5.Add(new Update() { Ids = (j + 1).ToString(), Last = test1[j].CompleteError, Next = test2[j].CompleteError });
                                up++;
                            }

                            if (test1[j].AccessError != test2[j].AccessError)
                            {
                                test5.Add(new Update() { Ids = (j + 1).ToString(), Last = test1[j].AccessError, Next = test2[j].AccessError });
                                up++;
                            }
                        }

                        if (up > 0)
                        {
                            MessageBox.Show("Успешно", "Проверка обновления");
                            MessageBox.Show($"Общее количестов обновлений: {up}", "Количество обновлений");

                            if (File.Exists(path + lab2))
                            {
                                File.Delete(path + lab2);
                            }

                            if (File.Exists(path + lab1))
                            {
                                File.Delete(path + lab1);
                            }

                            if (!File.Exists(path + lab2))
                            {
                                client.DownloadFile("https://bdu.fstec.ru/files/documents/thrlist.xlsx", "Laba2.xlsx");
                            }
                            upd = true;
                            Exel_Grid.ItemsSource = test5;
                            Exel_Grid.Items.Refresh();
                        }
                        else
                        {
                            MessageBox.Show("Нечего обновлять", "Обновление");
                            if (File.Exists(path + lab2))
                            {
                                if (shorts == false)
                                {
                                    Test[] test1_1 = EPPlus.ReadExcel(path + lab2);

                                    Test[] test2_1 = new Test[10];
                                    int k = 0;
                                    for (int i = (list - 1) * 10; i < 10 * list; i++)
                                    {
                                        test2_1[k] = test1_1[i];
                                        k++;
                                    }
                                    Exel_Grid.ItemsSource = test2_1;
                                    Exel_Grid.Items.Refresh();
                                }
                                else 
                                {
                                    Test2[] test4 = EPPlus.ShortReadExcel(path + lab2);

                                    Test2[] test6 = new Test2[10];
                                    int k = 0;
                                    for (int i = (list - 1) * 10; i < 10 * list; i++)
                                    {
                                        test6[k] = test4[i];
                                        k++;
                                    }

                                    Exel_Grid.ItemsSource = test6;
                                    Exel_Grid.Items.Refresh();
                                }
                            }

                            if (File.Exists(path + lab1))
                            {
                                File.Delete(path + lab1);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Файла нет!!!", "Ошибка");
                    if (File.Exists(path + lab1))
                    {
                        File.Delete(path + lab1);
                    }
                    upd = false;
                }
            }
            catch (Exception o)
            {
                MessageBox.Show(o.Message, "Возникла ошибка");
                if (File.Exists(path + lab1))
                {
                    File.Delete(path + lab1);
                }
            }
        }
    }
}
