﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba2
{
    public class Update
    {
        public string Id { get; set; }
        public string Last { get; set; }
        public string Next { get; set; }
    }
}
