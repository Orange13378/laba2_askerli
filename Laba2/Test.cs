﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba2
{
    public class Test
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
        public string Source { get; set; }
        public string Object { get; set; }
        public string PrivacyError { get; set; }
        public string CompleteError { get; set; }
        public string AccessError { get; set; }
    }
}
