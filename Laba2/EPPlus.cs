﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Laba2
{
    public class EPPlus
    {
        public EPPlus()
        {

        }

        public static Test[] ReadExcel(string FileName)
        {
            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var file = new FileInfo(FileName);
                using (var package = new ExcelPackage(file))
                {
                    using (ExcelWorksheet workSheet = package.Workbook.Worksheets[0])
                    {
                        Test[] products = new Test[workSheet.Dimension.End.Row - 2];
                        int countProduct = 0;
                        
                        var list1 = workSheet.Cells[workSheet.Dimension.Start.Row + 2, 1, workSheet.Dimension.End.Row, 8].ToList();

                        for (int i = 0; i < list1.Count; i += 8)
                        {
                            products[countProduct] = new Test();
                            products[countProduct].Id = list1[i].Text;
                            products[countProduct].Name = list1[i + 1].Text;
                            products[countProduct].Discription = list1[i + 2].Text;
                            products[countProduct].Source = list1[i + 3].Text;
                            products[countProduct].Object = list1[i + 4].Text;
                            products[countProduct].PrivacyError = list1[i + 5].Text;
                            products[countProduct].CompleteError = list1[i + 6].Text;
                            products[countProduct].AccessError = list1[i + 7].Text;
                            
                            countProduct++;
                        }
                        return products;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error read xslx!!!      " + ex.Message.ToString());
            }
            return null;
        }

        public static Test2[] ShortReadExcel(string FileName)
        {
            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var file = new FileInfo(FileName);
                
                using (var package = new ExcelPackage(file))
                {
                    using (ExcelWorksheet workSheet = package.Workbook.Worksheets[0])
                    {
                        Test2[] products = new Test2[workSheet.Dimension.End.Row - 2];
                        int countProduct = 0;
                        
                        var list1 = workSheet.Cells[workSheet.Dimension.Start.Row + 2, 1, workSheet.Dimension.End.Row, 2].ToList();

                        for (int i = 0; i < list1.Count; i += 2)
                        {
                            products[countProduct] = new Test2();
                            products[countProduct].Id = list1[i].Text;
                            products[countProduct].Name = list1[i + 1].Text;

                            countProduct++;
                        }
                        return products;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error read xslx!!!      " + ex.Message.ToString());
            }
            return null;
        }
    }
}
